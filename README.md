FLOW - Setup:

#1: User prooves they aren't a robot (CAPTCHA)
#2: User takes a selfie / video...
#3: Heirarchy generated from geolocation service written under /options
#4: Users in this bag are 'notified'

FLOW - One user says yes

#1: Person A swipes right
#2: The record /people/B/access/A = true is written
#3: Person B swipes left
#4: That's the end of that - person A has not provided location data.

FLOW - Both users say yes

#1: Person A swipes right
#2: The record /people/B/access/A = true is written
#3: Person B swiped right
#4: The record /people/A/access/B = true is written
#5: Person A is notified, and voluntarily writes to the record...
#6: The record /people/B/access/A = 'latLng=-123,234243' is written
#7: The record /people/A/access/B = 'latLng=-123,234243' is written

FLOW - Both users say yes, then one gets cold feet.

#PRE: Same as above
#1: Person A hits 'Cancel'
#2: The record /people/B/access/A = false is written
#3: Person B client picks this up and writes false as well.


https://maps.googleapis.com/maps/api/geocode/json?latlng=-27.4836,152.98943&sensor=false
{
	places: {
		"ChIJpba7S4lQkWsRQ6UmPfNbYYs": {
			users: {
				// should be "w4m": {}...
				'user-1234': { date: '' }
			}
		}
	},
	users: {
		'user-1234': { 
			'video': 'https://...'
		}
	}
}