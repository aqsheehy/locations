window.App = new AppController();

function AppController() {
    
    this.initialise = function(){
        
        firebase.initializeApp({
            apiKey: "AIzaSyD-cMv8UZ9U_MDvoFwJUScNy_9SImq0x2k",
            authDomain: "fluted-oath-134214.firebaseapp.com",
            databaseURL: "https://fluted-oath-134214.firebaseio.com",
            storageBucket: "fluted-oath-134214.appspot.com",
        }); 
        
        var options = [];
        var videoQueue = document.querySelector('.queue');
        getUser().then(function(user){
            
            var userVideoRef = dbUserVideo(user.uid);
            
            navigator.mediaDevices.getUserMedia({ video: true, audio: true }).then(function(media){
                
                var video = document.querySelector('video.profile');
                video.muted = true;
                video.src = window.URL.createObjectURL(media);
                video.play();
                
                var recorder = new MediaStreamRecorder(media);
                recorder.mimeType = 'video/webm';
                            
                recorder.ondataavailable = function (blob) {
                    
                    recorder.stop();
                    
                    uploadUserVideo(user.uid, blob).then(function(snapshot){
                        userVideoRef.set(snapshot.downloadURL);
                    });
                };
                                
                video.addEventListener('click', function(){
                    recorder.clearOldRecordedFrames();
                    recorder.start(5000);
                });
            });
            
            getUserGeocode().then(function(geocode){
                
                var geocodeUsers = dbGeocodeUsers(geocode);
                
                geocodeUsers.child(user.uid).set({ date: new Date().getTime() });
                
                geocodeUsers.once('value', function(users){
                    
                    for (var i in users.val()){
                        createVideoElement('/user/' + i + '/bideo.webm')
                            .then(function(video){
                                video.classList.add('user');
                                videoQueue.appendChild(video);
                            });
                    }
                });
                
            });
            
        });
    };
    
    // Bind public methods;
    this.initialise = (this.initialise).bind(this);
}

function createVideoElement(reference, type){
    return firebase.storage().ref(reference)
        .getDownloadURL()
        .then(function(url){
            
            var video = document.createElement('video');
            video.loop = true;
            
            video.addEventListener('click', function(ev){
                video.focus();
                document.querySelectorAll('.queue video:not(:focus)').forEach(function(v){
                    v.pause(); 
                });
                video.play();
            });
            
            var source = document.createElement('source');
            source.type = type || 'video/webm';
            source.src = url;
            video.appendChild(source);
            return video;
        });
}

function getUserGeocode() {
    return new Promise(function(resolve, reject){
        
        navigator.geolocation.getCurrentPosition(function(position){
            var http = new XMLHttpRequest();
            http.onreadystatechange = function(){
                if (http.readyState == 4 && http.status == 200) {
                    var geolocation = JSON.parse(http.responseText);
                    resolve(geolocation.results);
                }
            }
            
            var parameters = '?latlng='+ [position.coords.latitude, position.coords.longitude].join(); 
            http.open('GET', 'https://maps.googleapis.com/maps/api/geocode/json' + parameters);
            http.send();
        });
        
    })
}

function getUser(){
    
    return new Promise(function(resolve, reject){
        var auth = firebase.auth();
        auth.onAuthStateChanged(function(user) { 
            if (!user)
                auth.signInAnonymously().catch(function(err){
                    console.error(err);
                    reject(err);
                });
            else 
                resolve(user);
        }); 
    });
}

function uploadUserVideo(uid, blob){
    return new Promise(function(resolve, reject){
        var upload = firebase.storage().ref().child('/user/' + uid + '/bideo.webm').put(blob);
        upload.on(firebase.storage.TaskEvent.STATE_CHANGED, 
            function(snapshot){ console.log('Upload progress: ', snapshot); },
            function(e){ reject(e); },
            function() { resolve(upload.snapshot); });  
    });
}

function dbGeocodeUsers(geocode){
    var colloquial = _.find(geocode, function(place){
        return place.types.indexOf('colloquial_area') > -1;
    });
    return dbPlaceUsers(colloquial.place_id);
}

function dbUserVideo(uid){
    return firebase.database().ref().child('/users/' + uid + '/video');
}

function dbPlaceUsers(place_id){
    return firebase.database().ref().child('/places/' + place_id + '/users');
}